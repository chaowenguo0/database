import aiohttp, asyncio, argparse, asyncpg, builtins, pathlib, sys, tempfile, aiomysql, ssl, oci, cx_Oracle, io, zipfile, re

parser = argparse.ArgumentParser()
parser.add_argument('password')

configure = {'user':'ocid1.user.oc1..aaaaaaaalwudh6ys7562qtyfhxl4oji25zn6aapndqfuy2jfroyyielpu3pa', 'key_file':'oci.key', 'fingerprint':'bd:01:98:0d:5d:4a:6f:b2:49:b4:7f:df:43:00:32:39', 'tenancy':'ocid1.tenancy.oc1..aaaaaaaa4h5yoefhbxm4ybqy6gxl6y5cgxmdijira7ywuge3q4cbdaqnyawq', 'region':'us-sanjose-1'}
databaseClient = oci.database.DatabaseClient(configure)
databaseClientCompositeOperations = oci.database.DatabaseClientCompositeOperations(databaseClient)
password = 'ora1cle+ORAC'
dataWarehouse = oci.database.models.CreateAutonomousDatabaseBase(compartment_id=configure.get('tenancy'), db_name='dataWarehouse', admin_password=password, cpu_core_count=1, data_storage_size_in_tbs=1, db_workload='DW', is_free_tier=True)
transaction = oci.database.models.CreateAutonomousDatabaseBase(compartment_id=configure.get('tenancy'), db_name='transaction', admin_password=password, cpu_core_count=1, data_storage_size_in_tbs=1, db_workload='OLTP', is_free_tier=True)
#databaseClientCompositeOperations.create_autonomous_database_and_wait_for_state(dataWarehouse, wait_for_states=[oci.database.models.AutonomousDatabase.LIFECYCLE_STATE_AVAILABLE])
#databaseClientCompositeOperations.create_autonomous_database_and_wait_for_state(transaction, wait_for_states=[oci.database.models.AutonomousDatabase.LIFECYCLE_STATE_AVAILABLE])
generateAutonomousDatabaseWalletDetails = oci.database.models.GenerateAutonomousDatabaseWalletDetails(password=password)
for _ in databaseClient.list_autonomous_databases(compartment_id=configure.get('tenancy')).data:
    zipfile.ZipFile(io.BytesIO(databaseClient.generate_autonomous_database_wallet(_.id, generateAutonomousDatabaseWalletDetails).data.content)).extractall(_.id)
    tnsnames = pathlib.Path(_.id).joinpath('tnsnames.ora').read_text()
    connection = cx_Oracle.connect('admin', password, f"tcps://{re.search('(?<=host=)[.0-9a-z-]+', tnsnames).group(0)}:1522/{re.search('(?<=service_name=)[.0-9a-z_]+', tnsnames).group(0)}?wallet_location={_.id}")
    cursor = connection.cursor()
    cursor.execute(pathlib.Path('database.sql').read_text())
#https://www.oracle.com/database/technologies/instant-client.html

subscription = 'bc64100e-dcaf-4e19-b6ca-46872d453f08'

#az ad sp create-for-rbac --role Contributor --scopes /subscriptions/subscriptionID
#az login --service-principal -u appId -p password -t tenant
#az group delete -n postgres -y
#az group create -n postgres -l eastus
#az postgres flexible-server create -n postgresqlpostgres -g postgres -l eastus -u postgres -p 密码 -d default --public-access all --tier Burstable --sku-name Standard_B1ms --storage-size 32 --version 14
#PGPASSWORD=密码 psql -h postgresqlpostgres.postgres.database.azure.com -U postgres -d default -f database.sql
async def postgres(session, token):
    group = f'https://management.azure.com/subscriptions/{subscription}/resourcegroups/postgres?api-version=2021-04-01'
    async with session.head(group, headers={'authorization':f'Bearer {token}'}) as response:
        if response.status == 204:
            async with session.delete(group, headers={'authorization':f'Bearer {token}'}) as response:
                if response.status == 202:
                    while True:
                        await asyncio.sleep(builtins.int(response.headers.get('retry-after')))
                        async with session.get(response.headers.get('location'), headers={'authorization':f'Bearer {token}'}) as _:
                            if _.status == 200: break
    async with session.put(group, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus'}) as _: pass
    host = 'postgresqlpostgres'
    user = 'postgres'
    default = 'default'
    async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/postgres/providers/Microsoft.DBForPostgreSql/flexibleServers/{host}', params={'api-version':'2021-06-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus', 'sku':{'tier':'Burstable','name':'Standard_B1ms'}, 'properties':{'administratorLogin':user,'administratorLoginPassword':parser.parse_args().password, 'version':'14','storage':{'storageSizeGB':32}}}) as server:
        if server.status == 202:
            while True:
                await asyncio.sleep(builtins.int(server.headers.get('retry-after')))
                async with session.get(server.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
    async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/postgres/providers/Microsoft.DBForPostgreSql/flexibleServers/{host}/firewallRules/postgres', params={'api-version':'2021-06-01'}, headers={'authorization':f'Bearer {token}'}, json={'properties':{'startIpAddress':'0.0.0.0','endIpAddress':'255.255.255.255'}}) as firewall, session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/postgres/providers/Microsoft.DBForPostgreSql/flexibleServers/{host}/databases/{default}', params={'api-version':'2021-06-01'}, headers={'authorization':f'Bearer {token}'}, json={'properties':{'charset':'utf8','collation':'en_US.utf8'}}) as database:
        if firewall.status == 202:
            while True:
                await asyncio.sleep(builtins.int(firewall.headers.get('retry-after')))
                async with session.get(firewall.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
        if database.status == 202:
            while True:
                await asyncio.sleep(builtins.int(database.headers.get('retry-after')))
                async with session.get(database.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
    database = await asyncpg.create_pool(host=f'{host}.postgres.database.azure.com', user=user, database=default, password=parser.parse_args().password)
    await database.execute(pathlib.Path(__file__).resolve().parent.joinpath('database.sql').read_text())
    await database.close()

#az group delete -n mysql -y
#az group create -n mysql -l eastus
#az mysql flexible-server create -n mysqlsqlmy -g mysql -l eastus -u root -p 密码 -d default --public-access all --tier Burstable --sku-name Standard_B1ms --storage-size 32 --version 8.0.21
#mysql -h mysqlsqlmy.mysql.database.azure.com -uroot -p密码 -D default < database.sql
async def mysql(session, token):
    group = f'https://management.azure.com/subscriptions/{subscription}/resourcegroups/mysql?api-version=2021-04-01'
    async with session.head(group, headers={'authorization':f'Bearer {token}'}) as response:
        if response.status == 204:
            async with session.delete(group, headers={'authorization':f'Bearer {token}'}) as response:
                if response.status == 202:
                    while True:
                        await asyncio.sleep(builtins.int(response.headers.get('retry-after')))
                        async with session.get(response.headers.get('location'), headers={'authorization':f'Bearer {token}'}) as _:
                            if _.status == 200: break
    async with session.put(group, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus'}) as _: pass
    host = 'mysqlsqlmy'
    user = 'root'
    default = 'default'
    async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/mysql/providers/Microsoft.DBForMySql/flexibleServers/{host}', params={'api-version':'2021-05-01'},  headers={'authorization':f'Bearer {token}'}, json={'location':'eastus', 'sku':{'tier':'Burstable','name':'Standard_B1ms'}, 'properties':{'administratorLogin':user,'administratorLoginPassword':parser.parse_args().password, 'version':'8.0.21','storage':{'storageSizeGB':32}}}) as server:
        if server.status == 202:
            while True:
                await asyncio.sleep(builtins.int(server.headers.get('retry-after')))
                async with session.get(server.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
    async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/mysql/providers/Microsoft.DBForMySql/flexibleServers/{host}/firewallRules/mysql', params={'api-version':'2021-05-01'}, headers={'authorization':f'Bearer {token}'}, json={'properties':{'startIpAddress':'0.0.0.0','endIpAddress':'255.255.255.255'}}) as firewall, session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/mysql/providers/Microsoft.DBForMySql/flexibleServers/{host}/databases/{default}', params={'api-version':'2021-05-01'}, headers={'authorization':f'Bearer {token}'}, json={'properties':{'charset':'utf8','collation':'utf8_general_ci'}}) as database:
        if firewall.status == 202:
            while True:
                await asyncio.sleep(builtins.int(firewall.headers.get('retry-after')))
                async with session.get(firewall.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
        if database.status == 202:
            while True:
                await asyncio.sleep(builtins.int(database.headers.get('retry-after')))
                async with session.get(database.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
    with tempfile.NamedTemporaryFile(buffering=0) as pem:
        async with session.get('https://dl.cacerts.digicert.com/DigiCertGlobalRootCA.crt.pem') as _: pem.write(await _.content.read())
        ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        ctx.check_hostname = False
        ctx.load_verify_locations(cafile=pem.name)
        database = await aiomysql.create_pool(host=f'{host}.mysql.database.azure.com', user=user, db=default, password=parser.parse_args().password, sql_mode='ANSI_QUOTES', ssl=ctx)
        async with database.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(pathlib.Path(__file__).resolve().parent.joinpath('database.sql').read_text())
        database.close()
        await database.wait_closed()
    
async def main():
    async with aiohttp.ClientSession() as session:
        async with session.post('https://login.microsoftonline.com/476b320f-8c9c-46cb-8856-624a5d3c2c2c/oauth2/token', data={'grant_type':'client_credentials', 'client_id':'5ae868e9-2085-43a3-a3ae-9ff32a1b6b50', 'client_secret':'Sqh8Q~hIO_qqDHHoXXFJQJ1t4w7BPpW9B3hUNbbu', 'resource':'https://management.azure.com/'}) as response:
            token = (await response.json()).get('access_token')
            await asyncio.gather(sys.modules[__name__].postgres(session, token), sys.modules[__name__].mysql(session, token))
            
asyncio.run(main())